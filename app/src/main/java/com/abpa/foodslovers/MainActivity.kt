package com.abpa.foodslovers

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abpa.foodslovers.adapters.CategoriesRecyclerAdapter
import com.abpa.foodslovers.connectivity.Factory
import com.abpa.foodslovers.connectivity.MyApiEndpointInterface
import com.abpa.foodslovers.models.AllFood
import com.abpa.foodslovers.models.Foodlovers
import com.abpa.foodslovers.utils.Methods
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.gms.ads.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : AppCompatActivity() {
    var mRecyclerView: RecyclerView? = null
    var mAdapter: CategoriesRecyclerAdapter? = null
    var mAdView: AdView? = null
    var mRemoveAds: AppCompatButton? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fresco.initialize(this)
        mAdView = findViewById(R.id.adView)
        mRemoveAds = findViewById<View>(R.id.remove_ads) as AppCompatButton
        val noads = Methods.getPref(this@MainActivity, Methods.PREF_NO_ADS)
        if (noads != null && noads == "noads") {
            mRemoveAds!!.visibility = View.GONE
            mAdView!!.visibility = View.GONE
        } else {
            loadAdView()
        }
        mRecyclerView = findViewById<View>(R.id.allcategories_recyclerview) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(this)
        mRecyclerView!!.layoutManager = linearLayoutManager
        val mItemsList: MutableList<AllFood?> = ArrayList()
        mAdapter = CategoriesRecyclerAdapter(this@MainActivity, mItemsList)
        mRecyclerView!!.adapter = mAdapter
        val apiService: MyApiEndpointInterface = Factory.Companion.create()
        val call = apiService.recipes
        call!!.enqueue(object : Callback<Foodlovers?> {
            override fun onResponse(call: Call<Foodlovers?>, response: Response<Foodlovers?>) {
                if (response.code() == 200) {
                    val foodlovers = response.body()
                    mItemsList.clear()
                    if (foodlovers != null) {
                        foodlovers.allFoods?.forEach { mItemsList.add(it) }

                    }
                    mAdapter!!.notifyDataSetChanged()
                } else {
                }
            }

            override fun onFailure(call: Call<Foodlovers?>, t: Throwable) {
                Log.d("canceled", t.message!!)
            }
        })
    }

    fun loadAdView() {
        MobileAds.initialize(this) { }
        val adRequest = AdRequest.Builder().build()
        mAdView!!.loadAd(adRequest)
        mAdView!!.adListener = object : AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            override fun onAdFailedToLoad(adError: LoadAdError) {
                // Code to be executed when an ad request fails.
            }

            override fun onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        }
        mRemoveAds!!.setOnClickListener {
            AlertDialog.Builder(this@MainActivity)
                    .setTitle("Remove Ads")
                    .setMessage("Are you sure you want to remove Ads?") // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.yes) { dialog, which ->
                        Methods.savePre(this@MainActivity, "noads", Methods.PREF_NO_ADS)
                        mAdView!!.visibility = View.GONE
                        mRemoveAds!!.visibility = View.GONE
                    } // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
        }
    }
}