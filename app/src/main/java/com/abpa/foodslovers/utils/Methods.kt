package com.abpa.foodslovers.utils

import android.content.Context

object Methods {
    const val PREFS_NAME = "FoodLovers"
    const val PREF_NO_ADS = "pref_no_ads"
    fun getPref(c: Context, STOTREDkey: String?): String? {
        var mobileNum = ""
        try {
            val settings = c.getSharedPreferences(PREFS_NAME, 0)
            mobileNum = settings.getString(STOTREDkey, "").toString()
        } catch (ex: Exception) {
        }
        return mobileNum
    }

    fun savePre(c: Context, value: String?, key: String?): Boolean {
        if (value == null) return false
        if (value.length > 0) {
            val settings = c.getSharedPreferences(PREFS_NAME, 0)
            val editor = settings.edit()
            editor.putString(key, value)
            editor.commit()
        }
        return true
    }
}