package com.abpa.foodslovers.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

@Parcel(Parcel.Serialization.BEAN)
class Receipe {
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("imageurl")
    @Expose
    var imageurl: String? = null

    @SerializedName("timetoprepare")
    @Expose
    var timetoprepare: String? = null

    @SerializedName("smalldescription")
    @Expose
    var smalldescription: String? = null

    @SerializedName("ingredients")
    @Expose
    var ingredients: List<String>? = null

    @SerializedName("steps")
    @Expose
    var steps: List<String>? = null
}