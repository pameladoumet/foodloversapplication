package com.abpa.foodslovers.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

@Parcel(Parcel.Serialization.BEAN)
class Foodlovers {
    @SerializedName("AllFoods")
    @Expose
    var allFoods: List<AllFood>? = null
}