package com.abpa.foodslovers.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

@Parcel(Parcel.Serialization.BEAN)
class AllFood {
    @SerializedName("categoryName")
    @Expose
    var categoryName: String? = null

    @SerializedName("receipes")
    @Expose
    var receipes: List<Receipe>? = null
}