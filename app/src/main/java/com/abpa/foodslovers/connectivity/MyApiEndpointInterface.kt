package com.abpa.foodslovers.connectivity

import com.abpa.foodslovers.models.Foodlovers
import retrofit2.Call
import retrofit2.http.GET

interface MyApiEndpointInterface {
    // Request method and URL specified in the annotation
    @get:GET("api/v1/response/")
    val recipes: Call<Foodlovers?>?
}