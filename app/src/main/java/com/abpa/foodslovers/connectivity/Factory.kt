package com.abpa.foodslovers.connectivity

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Factory {
    var gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()
    var retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    companion object {
        const val BASE_URL = "http://testtask.solidtechapps.com/"
        fun create(): MyApiEndpointInterface {
            val builder = OkHttpClient().newBuilder()
            builder.readTimeout(5, TimeUnit.MINUTES)
            builder.connectTimeout(5, TimeUnit.MINUTES)
            //builder.addInterceptor(new HeaderInterceptor());
//        System.out.println(">>>>>>>>>>>>>>>>>>PUSH_ID>>>>>>>>>>>===\n\n" + Methods.getStringFromPrefs(ApplicationContext.getContext(), Config.PREF_KEY_GCM_PUSHID));
            builder.networkInterceptors().add(Interceptor { chain ->
                var request: Request? = null
                try {
//
                    request = chain.request().newBuilder().build()
                    //
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                chain.proceed(request)
            })
            val client = builder.build()
            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            return retrofit.create(MyApiEndpointInterface::class.java)
        }
    }
}