package com.abpa.foodslovers.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abpa.foodslovers.DetailsActivity
import com.abpa.foodslovers.R
import com.abpa.foodslovers.models.Receipe
import com.google.gson.Gson

class RecipesRecyclerAdapter(var mContext: Context, var mItemsList: List<Receipe?>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.layout_recipe_row, parent, false)
        return RecipeViewHolder.Companion.newInstance(view)
    }


    override fun getItemCount(): Int {
        return mItemsList!!.size
    }



    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as RecipeViewHolder
        viewHolder.mTitleTv.text = mItemsList!![position]?.name ?: null
        viewHolder.mRecipeIv.setImageURI(Uri.parse(mItemsList!![position]?.imageurl))
        viewHolder.mTimeTv.text = mItemsList!![position]?.timetoprepare ?: null
        viewHolder.mDescriptionTv.text = mItemsList!![position]?.smalldescription ?: null
        viewHolder.mCardView.setOnClickListener {
            val i = Intent(mContext, DetailsActivity::class.java)
            val gson = Gson()
            val result = gson.toJson(mItemsList!![viewHolder.adapterPosition])
            i.putExtra(DetailsActivity.Companion.EXTRA_RECIPE, result)
            mContext.startActivity(i)
        }
    }
}
