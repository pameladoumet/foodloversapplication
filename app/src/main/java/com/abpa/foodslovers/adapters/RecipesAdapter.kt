package com.abpa.foodslovers.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abpa.foodslovers.R
import com.abpa.foodslovers.models.AllFood

class RecipesAdapter(private val mModelList: List<AllFood>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    fun isHeader(position: Int): Boolean {
        return position == 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType == ITEM_VIEW_TYPE_HEADER) {
            val headerView = inflater.inflate(R.layout.layout_header, parent, false)
            return HeaderViewHolder(headerView)
        }
        val cellView = inflater.inflate(R.layout.layout_recipe_row, parent, false)
        return RecipeViewHolder.Companion.newInstance(cellView)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isHeader(position)) ITEM_VIEW_TYPE_HEADER else ITEM_VIEW_TYPE_ITEM
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, position: Int) {
        if (isHeader(position)) {
            return
        }

        // final YourModel model = mModelList.get(position -1 ); // Subtract 1 for header

        //  ModelHolder holder = (ModelHolder) h;
        // populate your holder with data from your model as usual
    }

    override fun getItemCount(): Int {
        //  return _categories.size() + 1; // add one for the header
        return 0
    }

    companion object {
        private const val ITEM_VIEW_TYPE_HEADER = 0
        private const val ITEM_VIEW_TYPE_ITEM = 1
    }
}