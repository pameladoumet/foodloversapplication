package com.abpa.foodslovers.adapters

import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.abpa.foodslovers.R

class AllRecipesViewHolder(itemView: View, var mRecyclerLayout: RelativeLayout, var mTitleTv: AppCompatTextView, var mRecyclerView: RecyclerView) : RecyclerView.ViewHolder(itemView) {
    companion object {
        fun newInstance(itemView: View): AllRecipesViewHolder {
            val categoryTv = itemView.findViewById<View>(R.id.header_tv) as AppCompatTextView
            val recyclerView = itemView.findViewById<View>(R.id.grid_recyclerview) as RecyclerView
            val recyclerLayout = itemView.findViewById<View>(R.id.recycler_layout) as RelativeLayout
            return AllRecipesViewHolder(itemView, recyclerLayout, categoryTv, recyclerView)
        }
    }
}