package com.abpa.foodslovers.adapters

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.abpa.foodslovers.R
import com.facebook.drawee.view.SimpleDraweeView

class RecipeViewHolder(itemView: View, var mCardView: CardView, var mRecipeIv: SimpleDraweeView, var mTitleTv: AppCompatTextView,
                       var mTimeTv: AppCompatTextView, var mDescriptionTv: AppCompatTextView) : RecyclerView.ViewHolder(itemView) {
    companion object {
        fun newInstance(itemView: View): RecipeViewHolder {
            val recipeIv = itemView.findViewById<View>(R.id.recipe_iv) as SimpleDraweeView
            val titleTv = itemView.findViewById<View>(R.id.title_tv) as AppCompatTextView
            val timeTv = itemView.findViewById<View>(R.id.time_tv) as AppCompatTextView
            val descriptionTv = itemView.findViewById<View>(R.id.description_tv) as AppCompatTextView
            val cardView = itemView.findViewById<View>(R.id.cardview) as CardView
            return RecipeViewHolder(itemView, cardView, recipeIv, titleTv, timeTv, descriptionTv)
        }
    }
}