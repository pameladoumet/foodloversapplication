package com.abpa.foodslovers.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abpa.foodslovers.R
import com.abpa.foodslovers.models.AllFood

class CategoriesRecyclerAdapter(var mContext: Context, var mItemsList: List<AllFood?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.layout_header_recycler, parent, false)
        return AllRecipesViewHolder.Companion.newInstance(view)
    }



    override fun getItemCount(): Int {
        return mItemsList.size
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as AllRecipesViewHolder
        viewHolder.mTitleTv.text = mItemsList[position]?.categoryName ?: null
        val mAdapter = RecipesRecyclerAdapter(mContext, mItemsList[position]?.receipes)
        val gridLayoutManager = GridLayoutManager(mContext, 2)
        viewHolder.mRecyclerView.layoutManager = gridLayoutManager
        viewHolder.mRecyclerView.adapter = mAdapter
    }
}