package com.abpa.foodslovers.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    companion object {
        fun newInstance(itemView: View): HeaderViewHolder {
            return HeaderViewHolder(itemView)
        }
    }
}