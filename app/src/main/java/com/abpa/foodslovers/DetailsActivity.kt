package com.abpa.foodslovers

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.abpa.foodslovers.DetailsActivity
import com.abpa.foodslovers.models.Receipe
import com.abpa.foodslovers.utils.Methods
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.gson.Gson

class DetailsActivity : AppCompatActivity() {
    var ingredientsTv: AppCompatTextView? = null
    var directionsTv: AppCompatTextView? = null
    var mRecipeIv: SimpleDraweeView? = null
    private var mInterstitialAd: InterstitialAd? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        ingredientsTv = findViewById<View>(R.id.ingredients_tv) as AppCompatTextView
        directionsTv = findViewById<View>(R.id.directions_tv) as AppCompatTextView
        mRecipeIv = findViewById<View>(R.id.recipe_iv) as SimpleDraweeView
        val result = intent.getStringExtra(EXTRA_RECIPE)
        val gson = Gson()
        val recipe = gson.fromJson<Receipe>(result, Receipe::class.java) as Receipe
        ingredientsTv!!.text = getIngredients(recipe.ingredients)
        directionsTv!!.text = getDirections(recipe.steps)
        mRecipeIv!!.setImageURI(Uri.parse(recipe.imageurl))
        supportActionBar!!.setTitle(recipe.name)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun getIngredients(ingredients: List<String?>?): String {
        var result = ""
        if (ingredients != null) {
            ingredients.forEach {
                result +=
                "-" + it +"\n"
            }
        }

        return result
    }

    fun getDirections(directions: List<String?>?): String {
        var result = ""
        var index=0
        if (directions != null) {
            directions.forEach {
                index++
                result += "Step"+index  +"\n"
                result += it  +"\n"
 

            }
        }
        return result
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                val noads = Methods.getPref(this@DetailsActivity, Methods.PREF_NO_ADS)
                if (noads != null && noads == "noads") {
                } else {
                    loadAd()
                }
                return true
            }
        }
        return false
    }

    fun loadAd() {
        MobileAds.initialize(this) { }
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, "ca-app-pub-3940256099942544/1033173712", adRequest,
                object : InterstitialAdLoadCallback() {
                    override fun onAdLoaded(interstitialAd: InterstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd
                        if (mInterstitialAd != null) {
                            mInterstitialAd!!.show(this@DetailsActivity)
                        } else {
                            Log.d("TAG", "The interstitial ad wasn't ready yet.")
                        }
                        Log.i("TAG", "onAdLoaded")
                    }

                    override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                        // Handle the error
                        Log.i("TAG", loadAdError.message)
                        mInterstitialAd = null
                    }
                })
        if (mInterstitialAd != null) {
            mInterstitialAd!!.show(this@DetailsActivity)
        } else {
            Log.d("TAG", "The interstitial ad wasn't ready yet.")
        }
    }

    companion object {
        const val EXTRA_RECIPE = "recipe"
    }
}